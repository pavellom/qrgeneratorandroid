package org.pavellom.qrgenerator.app

import android.app.Application
import android.util.Log
import org.pavellom.qrgenerator.data.Items
import org.pavellom.qrgenerator.data.ViewMode
import org.pavellom.qrgenerator.fragment.*

/**
* Created by pavellomonosov on 23/12/2017.
*/
class Injector : Application() {

    val TAG = "Injector"

    override fun onCreate() {
        super.onCreate()
        Log.i(TAG, "app created")

        items = Items()

        // Toolbar view
        viewMode = ViewMode(true)

        // Fragments
        qrForm = QrForm.newInstance()
        qrWifiForm = QrWifiForm.newInstance()
        qrResult = QRResult.newInstance()
        qrVcard = QrVcardForm.newInstance()
        qrYoutube = QrYoutubeForm.newInstance()
        qrUrl = QrUrlForm.newInstance()

        Log.i(TAG, "Loaded app objects")
    }

    companion object {
        private lateinit var items: Items

        // Toolbar data object
        private lateinit var viewMode: ViewMode

        // Fragments
        private lateinit var qrForm: ExtendFragment
        private lateinit var qrWifiForm: ExtendFragment
        private lateinit var qrResult: QRResult
        private lateinit var qrVcard: QrVcardForm
        private lateinit var qrYoutube: QrYoutubeForm
        private lateinit var qrUrl: QrUrlForm

        fun injectItems() = items
        fun injectQrForm() = qrForm
        fun injectQrWifiForm() = qrWifiForm
        fun injectQrResult() = qrResult
        fun injectQrVcardForm() = qrVcard
        fun injectQrYoutubeForm() = qrYoutube
        fun injectQrUrlForm() = qrUrl

        fun injectViewMode() = viewMode
    }
}