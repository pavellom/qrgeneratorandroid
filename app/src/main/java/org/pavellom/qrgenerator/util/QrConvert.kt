package org.pavellom.qrgenerator.util

import android.graphics.Bitmap
import net.glxn.qrgen.android.QRCode
import org.pavellom.qrgenerator.R

class QrConvert {
    companion object {
        fun toBitmap(data: String, dim: Int): Bitmap {
            return QRCode.from(data).withSize(dim, dim).bitmap()
        }
    }
}