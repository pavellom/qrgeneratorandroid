package org.pavellom.qrgenerator.util

/**
 * Created by pavellomonosov on 21/03/2018.
 */
class CommonUtils{
    companion object {
        fun isNotEmptyStr(str: String?):Boolean{
            return str != null && str.trim().length > 0
        }
        fun isEmptyStr(str: String?):Boolean{
            return !isNotEmptyStr(str);
        }
    }
}