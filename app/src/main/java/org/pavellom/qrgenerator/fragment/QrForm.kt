package org.pavellom.qrgenerator.fragment

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_qr_form.*
import org.pavellom.qrgenerator.R
import org.pavellom.qrgenerator.data.Event
import org.pavellom.qrgenerator.data.TextForm
import org.pavellom.qrgenerator.databinding.FragmentQrFormBinding
import org.pavellom.qrgenerator.enum.ItemType
import org.pavellom.qrgenerator.util.CommonUtils

class QrForm : ExtendFragment() {

    private lateinit var textForm: TextForm

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val inflate = DataBindingUtil.inflate<FragmentQrFormBinding>(inflater, R.layout.fragment_qr_form, container, false)
        val view = inflate.root
        textForm = TextForm()
        inflate.textForm = textForm
        return view
    }

    override fun onStart() {
        super.onStart()
        updateActivity(Event(Event.FORM_VALID).addArg("valid", false))
        qrText.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {

                if (CommonUtils.isNotEmptyStr(s.toString()) != isValidForm){
                    isValidForm = !isValidForm
                    updateActivity(Event(Event.FORM_VALID).addArg("valid", isValidForm))
                }
            }
        })
    }

    override fun generateQr(): String? {
        return textForm.text
    }

    override fun suffix(): String? {
        return ItemType.TEXT.toString()
    }

    companion object {
        fun newInstance(): QrForm {
            return QrForm()
        }
    }
}
