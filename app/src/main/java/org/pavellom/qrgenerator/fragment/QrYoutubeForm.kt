package org.pavellom.qrgenerator.fragment


import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_qr_youtube_form.*
import org.pavellom.qrgenerator.R
import org.pavellom.qrgenerator.data.Event
import org.pavellom.qrgenerator.data.Youtube
import org.pavellom.qrgenerator.databinding.FragmentQrYoutubeFormBinding
import org.pavellom.qrgenerator.enum.ItemType
import org.pavellom.qrgenerator.util.CommonUtils


/**
 * A simple [Fragment] subclass.
 */
class QrYoutubeForm : ExtendFragment() {

    private lateinit var youtube: Youtube

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val inflate = DataBindingUtil.inflate<FragmentQrYoutubeFormBinding>(inflater, R.layout.fragment_qr_youtube_form, container, false)
        val view = inflate.root
        youtube = Youtube()
        inflate.youtube = youtube

        return view;
    }

    override fun onStart() {
        super.onStart()
        youtube_url.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
                val youtubeObjectId = getYoutubeObjectId(s)
                if (CommonUtils.isNotEmptyStr(youtubeObjectId) != isValidForm){
                    isValidForm = !isValidForm
                    updateActivity(Event(Event.FORM_VALID).addArg("valid", isValidForm))
                }
            }
        })
    }

    override fun generateQr(): String? {
        return "youtube://" + getYoutubeObjectId(youtube.url)
    }

    private fun getYoutubeObjectId(url: CharSequence): String?{
        val regex = Regex("^http[s]?:\\/\\/.+(\\?v=|\\/)(.+)$")
        val youtubeStr: String? = regex.matchEntire(url)?.groups?.get(2)?.value
        return youtubeStr
    }

    override fun suffix(): String? {
        return ItemType.YOUTUBE.toString()
    }

    companion object {
        fun newInstance(): QrYoutubeForm {
            return QrYoutubeForm()
        }
    }
}
