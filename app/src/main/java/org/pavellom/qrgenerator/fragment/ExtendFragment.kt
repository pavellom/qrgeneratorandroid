package org.pavellom.qrgenerator.fragment

import android.content.Context
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.view.View
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import org.pavellom.qrgenerator.data.Event

/**
 * Created by pavellomonosov on 23/12/2017.
 */
open class ExtendFragment : Fragment() {

    open var mListener: OnFragmentInteractionListener? = null

    val subscriptions = CompositeDisposable()

    protected var isValidForm: Boolean = false

    fun subscribe(disposable: Disposable): Disposable {
        subscriptions.add(disposable)
        return disposable
    }

    fun updateActivity(event: Event) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(event)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    open fun generateQr(): String? {
        return null
    }

    open fun suffix(): String? {
        return null
    }

    override fun onResume() {
        super.onResume()
        val event = Event(Event.UPDATE_PROGRESS).addArg("fragment", this)
        updateActivity(event)
    }

    override fun onStop() {
        super.onStop()
        subscriptions.clear()
    }

    fun <T : View> View.bind(@IdRes idRes: Int): Lazy<T> {
        return unsafeLazy { findViewById<T>(idRes) as T }
    }

    private fun <T> unsafeLazy(initializer: () -> T) = lazy(LazyThreadSafetyMode.NONE, initializer)

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(event: Event)
    }
}