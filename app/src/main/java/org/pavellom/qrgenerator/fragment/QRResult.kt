package org.pavellom.qrgenerator.fragment

import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import org.pavellom.qrgenerator.R
import org.pavellom.qrgenerator.file.LoadFiles

class QRResult : ExtendFragment() {

    var bitmap: Bitmap? = null
    var fileName: String?

    private var screenWidth: Int

    init {
        screenWidth = 0
        fileName = null
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_qrresult, container, false)
        updateUI(view)
        return view
    }

    fun updateUI(view: View) {
        val imageView by view.bind<ImageView>(R.id.qrPreview)
        imageView.setImageBitmap(if (bitmap == null) LoadFiles.load(fileName) else bitmap!!)
    }

    companion object {
        fun newInstance(): QRResult {
            return QRResult()
        }
    }
}
