package org.pavellom.qrgenerator.fragment

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import net.glxn.qrgen.core.scheme.VCard
import org.pavellom.qrgenerator.R
import org.pavellom.qrgenerator.data.Event
import org.pavellom.qrgenerator.data.Vcard
import org.pavellom.qrgenerator.databinding.FragmentQrVcardFormBinding
import org.pavellom.qrgenerator.enum.ItemType

/**
 * Created by pavellomonosov on 16/02/2018.
 */
class QrVcardForm : ExtendFragment() {

    private lateinit var vcard: Vcard

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val inflate = DataBindingUtil.inflate<FragmentQrVcardFormBinding>(inflater, R.layout.fragment_qr_vcard_form, container, false)
        val view = inflate.root
        vcard = Vcard()
        inflate.vcard = vcard
        return view;
    }

    override fun generateQr(): String? {
        val vCard = VCard()
        vCard.address = vcard.address
        vCard.company = vcard.company
        vCard.email = vcard.email
        vCard.name = vcard.name
        vCard.note = vcard.note
        vCard.phoneNumber = vcard.phoneNumber
        vCard.title = vcard.title
        vCard.website = vcard.website

        return vCard.toString()
    }

    override fun onStart() {
        super.onStart()
        updateActivity(Event(Event.FORM_VALID).addArg("valid", true))
    }

    override fun suffix(): String? {
        return ItemType.CONTACT.toString()
    }

    companion object {
        fun newInstance(): QrVcardForm {
            return QrVcardForm()
        }
    }
}