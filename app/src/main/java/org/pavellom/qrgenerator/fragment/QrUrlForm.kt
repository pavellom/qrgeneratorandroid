package org.pavellom.qrgenerator.fragment


import android.databinding.DataBindingUtil
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_qr_url_form.*
import org.pavellom.qrgenerator.R
import org.pavellom.qrgenerator.data.Event
import org.pavellom.qrgenerator.data.Url
import org.pavellom.qrgenerator.databinding.FragmentQrUrlFormBinding
import org.pavellom.qrgenerator.enum.ItemType
import org.pavellom.qrgenerator.util.CommonUtils
import java.util.regex.Pattern

class QrUrlForm : ExtendFragment() {

    private lateinit var url: Url


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val inflate = DataBindingUtil.inflate<FragmentQrUrlFormBinding>(inflater, R.layout.fragment_qr_url_form, container, false)
        val view = inflate.root
        url = Url()
        inflate.url = url
        return view;
    }

    override fun onStart() {
        super.onStart()
        updateActivity(Event(Event.FORM_VALID).addArg("valid", false))
        url_value.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {

                if (CommonUtils.isNotEmptyStr(s.toString()) && isValidUrl(s.toString()) != isValidForm) {
                    isValidForm = !isValidForm
                    updateActivity(Event(Event.FORM_VALID).addArg("valid", isValidForm))
                }
            }
        })
    }

    override fun generateQr(): String? {
        return url.url
    }

    override fun suffix(): String? {
        return ItemType.URL.toString()
    }

    companion object {
        fun newInstance(): QrUrlForm {
            return QrUrlForm()
        }
    }

    fun isValidUrl(email: String): Boolean {
        return Pattern.compile("^(http|https):\\/\\/+[\\www\\d]+\\.[\\w]+(\\/[\\w\\d]+)?"
        ).matcher(email).matches()
    }
}
