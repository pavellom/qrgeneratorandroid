package org.pavellom.qrgenerator.fragment

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import kotlinx.android.synthetic.main.fragment_qr_wifi_form.*
import net.glxn.qrgen.core.scheme.Wifi
import net.glxn.qrgen.core.scheme.Wifi.Authentication.*
import org.pavellom.qrgenerator.R
import org.pavellom.qrgenerator.data.Event
import org.pavellom.qrgenerator.data.WifiFormState
import org.pavellom.qrgenerator.databinding.FragmentQrWifiFormBinding
import org.pavellom.qrgenerator.enum.ItemType
import org.pavellom.qrgenerator.enum.WifiForm
import org.pavellom.qrgenerator.enum.WifiForm.NOPASS
import org.pavellom.qrgenerator.util.CommonUtils.Companion.isEmptyStr
import org.pavellom.qrgenerator.util.CommonUtils.Companion.isNotEmptyStr

class QrWifiForm : ExtendFragment() {

    private lateinit var currentState: WifiFormState

    private var validFlag: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val inflate = DataBindingUtil.inflate<FragmentQrWifiFormBinding>(inflater, R.layout.fragment_qr_wifi_form, container, false)
        val view = inflate.root
        currentState = WifiFormState(WifiForm.NOPASS.label, WifiForm.NOPASS.visiblity, "", "")
        inflate.state = currentState
        view.findViewById<Button>(R.id.auth_type).setOnClickListener {
            changeAuth(view)
        }
        return view;
    }

    fun changeAuth(view: View) {
        var order = WifiForm.values().find { it.label.equals(currentState.label) }?.ordinal!!
        order = if (order == WifiForm.values().size - 1) 0 else order + 1
        currentState.label = WifiForm.values().get(order).label
        currentState.visibility = WifiForm.values().get(order).visiblity
        currentState.notifyChange()
        validFlag = isNotEmptyStr(currentState.ssid) && !(isEmptyStr(currentState.password) && !currentState.label.equals(NOPASS.label))
        updateActivity(Event(Event.FORM_VALID).addArg("valid", validFlag))
    }

    override fun generateQr(): String? {
        val authentication = getAuthentication()
        val wifi = Wifi()
        wifi.authentication = authentication.toString()
        wifi.ssid = currentState.ssid
        if (!authentication.equals(nopass)) {
            wifi.psk = currentState.password
        }

        return wifi.toString()
    }

    fun getAuthentication(): Wifi.Authentication {
        return when (currentState.label) {
            WifiForm.WEP.toString() -> WEP
            WifiForm.WPA.toString() -> WPA
            else -> {
                nopass
            }
        }
    }

    override fun onStart() {
        super.onStart()
        updateActivity(Event(Event.FORM_VALID).addArg("valid", false))
        ssid_name.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {

                if (isEmptyStr(s.toString())) {
                    isValidForm = false

                } else {
                    isValidForm = !(isEmptyStr(currentState.password) && !currentState.label.equals(NOPASS.label))
                }

                updateActivity(Event(Event.FORM_VALID).addArg("valid", isValidForm))
            }
        })

        password.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {

                if (!currentState.label.equals(NOPASS.label) && isEmptyStr(s.toString())) {
                    isValidForm = false
                } else {
                    isValidForm = isNotEmptyStr(currentState.ssid);
                }

                updateActivity(Event(Event.FORM_VALID).addArg("valid", isValidForm))
            }
        })
    }

    override fun suffix(): String? {
        return ItemType.WIFI.toString()
    }

    companion object {
        fun newInstance(): QrWifiForm {
            return QrWifiForm()
        }
    }
}
