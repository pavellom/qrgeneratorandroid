package org.pavellom.qrgenerator.file

import android.content.Context
import android.content.ContextWrapper
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import org.pavellom.qrgenerator.app.Injector
import org.pavellom.qrgenerator.data.Item
import org.pavellom.qrgenerator.enum.ItemType
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

/**
 * Created by pavellomonosov on 09/01/2018.
 */
class LoadFiles {

    companion object {
        private val items = Injector.injectItems()

        fun load(context: Context) {
            items.reset()
            val cw = ContextWrapper(context)
            cw.filesDir?.listFiles()?.forEach {
                if (it.absolutePath.endsWith("png")) {
                    items.addItem(Item.builder().itemType(ItemType.TEXT.name).path(it.absolutePath).img(Bitmap.createScaledBitmap(BitmapFactory.decodeFile(it.absolutePath),
                            50, 50, false)).build())
                }
            }
        }

        fun load(path: String?): Bitmap? = if (path != null) BitmapFactory.decodeFile(path) else null

        fun deleteAll(context: Context) {
            val cw = ContextWrapper(context)
            cw.filesDir?.listFiles()?.forEach { it.delete() }
            items.reset()
        }

        fun delete(context: Context, item: Item) {
            val cw = ContextWrapper(context)
            cw.deleteFile(getFileName(item))
            items.removeItem(item)
        }

        fun getFileName(item: Item): String {
            val paths = item.path().split(File.separator)
            return paths[paths.size - 1]
        }

        fun saveToInternalStorage(context: Context, bitmapImage: Bitmap, name: String): String {
            context.openFileOutput(name, Context.MODE_PRIVATE).use {
                bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, it)
                return context.getFileStreamPath(name).absolutePath
            }
        }
    }
}