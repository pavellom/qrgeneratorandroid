package org.pavellom.qrgenerator.enum

import org.pavellom.qrgenerator.R
import org.pavellom.qrgenerator.app.Injector
import org.pavellom.qrgenerator.fragment.ExtendFragment

/**
 * Created by pavellomonosov on 09/01/2018.
 */
enum class ItemType(val resourceId: Int, val fillForm: ExtendFragment) {
    TEXT(R.drawable.ic_title_black_24dp, Injector.injectQrForm()),
    WIFI(R.drawable.ic_wifi_black_24dp, Injector.injectQrWifiForm()),
    URL(R.drawable.ic_http_black_24dp, Injector.injectQrUrlForm()),
    CONTACT(R.drawable.ic_contact_phone_black_24dp, Injector.injectQrVcardForm()),
//    EMAIL(R.drawable.ic_email_black_24dp, Injector.injectQrType()),
//    PHONE(R.drawable.ic_call_black_24dp, Injector.injectQrType()),
//    SMS(R.drawable.ic_textsms_black_24dp, Injector.injectQrType()),
    YOUTUBE(R.drawable.ic_subscriptions_black_24dp, Injector.injectQrYoutubeForm())
}