package org.pavellom.qrgenerator.enum

import android.view.View

/**
 * Created by pavellomonosov on 09/01/2018.
 */
enum class WifiForm(val visiblity: Int, val label: String) {
    WEP(View.VISIBLE, "WEP"),
    WPA(View.VISIBLE, "WPA"),
    NOPASS(View.INVISIBLE, "NOPASS")
}