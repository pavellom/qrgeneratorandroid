package org.pavellom.qrgenerator.activity

import android.app.Activity
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.View
import android.view.inputmethod.InputMethodManager
import org.pavellom.qrgenerator.R


/**
 * Created by pavellomonosov on 23/12/2017.
 */
open class ExtendActivity : AppCompatActivity() {

    val SUPER_TAG = "ExtendActivity"

    inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> FragmentTransaction) {
        beginTransaction().func().commit()
    }

    fun AppCompatActivity.addFragment(id: Int, fragment: Fragment) {
        supportFragmentManager.inTransaction { add(id, fragment) }
    }

    fun AppCompatActivity.replaceFragment(id: Int, fragment: Fragment) {
        supportFragmentManager.inTransaction { replace(id, fragment) }
    }

    fun <T : View> AppCompatActivity.bind(@IdRes idRes: Int): Lazy<T> {
        return unsafeLazy { findViewById<T>(idRes) as T }
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            Log.i(SUPER_TAG, "Back pressed: " + supportFragmentManager.backStackEntryCount)
            supportFragmentManager.popBackStack()
        } else {
            Log.i(SUPER_TAG, "Back pressed, blocked event")
        }
    }

    fun closeKeyboard(){
        val imm = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = this.getCurrentFocus()
        if (view == null) {
            view = View(this)
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0)
    }

    private fun <T> unsafeLazy(initializer: () -> T) = lazy(LazyThreadSafetyMode.NONE, initializer)
}