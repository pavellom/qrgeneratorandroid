package org.pavellom.qrgenerator.activity

import android.content.Intent
import android.databinding.BindingAdapter
import android.databinding.DataBindingUtil.setContentView
import android.graphics.Bitmap
import android.os.Bundle
import android.support.v4.content.FileProvider
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.ShareActionProvider
import android.util.DisplayMetrics
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.ImageButton
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import kotlinx.android.synthetic.main.activity_app.*
import org.pavellom.qrgenerator.R
import org.pavellom.qrgenerator.app.Injector
import org.pavellom.qrgenerator.data.Event
import org.pavellom.qrgenerator.data.Form
import org.pavellom.qrgenerator.data.Item
import org.pavellom.qrgenerator.databinding.ActivityAppBinding
import org.pavellom.qrgenerator.enum.ItemType
import org.pavellom.qrgenerator.file.LoadFiles
import org.pavellom.qrgenerator.fragment.ExtendFragment
import org.pavellom.qrgenerator.util.CommonUtils
import org.pavellom.qrgenerator.util.QrConvert
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import android.view.inputmethod.InputMethodManager.HIDE_IMPLICIT_ONLY
import android.app.Activity
import android.content.Intent.FLAG_ACTIVITY_SINGLE_TOP
import android.view.inputmethod.InputMethodManager


class AppActivity : ExtendActivity(),
        ExtendFragment.OnFragmentInteractionListener {

    val TAG = "AppActivity"

    private var screenWidth: Int? = 0

    val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH-mm-ss")

    private lateinit var currentType: Form

    private lateinit var mShareActionProvider: ShareActionProvider

    private var qrText: String? = null

    private lateinit var shareMenuItem: MenuItem

    private var qrResult = Injector.injectQrResult()

    init {
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    override fun onFragmentInteraction(event: Event) {
        when (event.eventType) {
            Event.FORM_VALID -> {
                currentType.generateActive = event.args.get("valid") as Boolean
                next_view.isSelected = currentType.generateActive
                currentType.notifyChange()
            }
        }
    }

    fun changeType(view: View) {
        var order = ItemType.values().find { it.resourceId.equals(currentType.imageSrc) }?.ordinal!!
        order = if (order == ItemType.values().size - 1) 0 else order + 1
        currentType.imageSrc = ItemType.values().get(order).resourceId
        currentType.notifyChange()
        replaceProgress(ItemType.values().get(order).fillForm)
        closeKeyboard()
    }

    private fun replaceProgress(fragment: ExtendFragment) {
        replaceFragment(R.id.fragmentId, fragment)
    }

    fun generateQR(data: Bitmap?, fileName: String?) {
        displayHeader(false)
        qrResult.bitmap = data
        qrResult.fileName = fileName
        qrText = fileName
        replaceProgress(qrResult)
    }

    fun generateQr(view: View) {
        displayHeader(false)
        val extendFragment = supportFragmentManager.findFragmentById(R.id.fragmentId) as ExtendFragment
        val qrText = extendFragment.generateQr()!!
        if (CommonUtils.isNotEmptyStr(qrText)) {
            val current = java.util.Calendar.getInstance()
            val bitmap = QrConvert.toBitmap(qrText, screenWidth!!)
            val name = extendFragment.suffix() + "-" + simpleDateFormat.format(current.getTime()) + ".png"
            val filePah = LoadFiles.saveToInternalStorage(this.applicationContext, bitmap, name)
            generateQR(null, filePah)
            setShareIntent(filePah)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_app)

        val bind = setContentView<ActivityAppBinding>(
                this,
                R.layout.activity_app
        )
        currentType = Form(ItemType.TEXT.resourceId, false)
        bind.form = currentType

        toolbar.title = ""
        setSupportActionBar(toolbar)

        initScale()

        MobileAds.initialize(this,
                getString(R.string.adMob_id))
        adView.loadAd(AdRequest.Builder().build())
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_home -> {
                val intent = Intent(this, MainActivity::class.java)
                intent.flags = intent.flags or Intent.FLAG_ACTIVITY_NO_HISTORY
                startActivity(intent)
                this.finish()
            }
        }
        return true
    }

    override fun onStart() {
        super.onStart()
        addFragment(R.id.fragmentId, ItemType.values().get(0).fillForm)

        val item = this.intent.getParcelableExtra("item") as Item?
        val filePah = if (item == null) qrText else item.path()
        if (filePah != null) {
            generateQR(null, filePah)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.items, menu)
        shareMenuItem = menu!!.findItem(R.id.action_share)
        mShareActionProvider = MenuItemCompat.getActionProvider(shareMenuItem) as ShareActionProvider
        setShareIntent(qrText)
        if (!qrResult.isAdded) {
            shareMenuItem.setVisible(false)
        }
        return true
    }

    private fun setShareIntent(filePah: String?) {
        closeKeyboard()
        shareMenuItem.setVisible(true)
        if (mShareActionProvider != null && CommonUtils.isNotEmptyStr(filePah)) {
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            shareIntent.type = "image/png"
            shareIntent.putExtra(Intent.EXTRA_STREAM,
                    FileProvider.getUriForFile(this, this.getApplicationContext().getPackageName() + ".provider", File(filePah)))
            mShareActionProvider.setShareIntent(shareIntent)
        }
    }

    fun displayHeader(show: Boolean) {
        val visibility: Int = if (show) View.VISIBLE else View.GONE
        choose_type_label.visibility = visibility
        next_view.visibility = visibility
        type_view.visibility = visibility
    }

    private fun initScale() {
        if (screenWidth == 0) {
            val displayMetrics = DisplayMetrics()
            this.windowManager
                    .defaultDisplay
                    .getMetrics(displayMetrics)
            screenWidth = displayMetrics.widthPixels
        }
    }
}

object ImageBindingAdapter {
    @JvmStatic
    @BindingAdapter("android:src")
    fun setImageSrc(view: ImageButton, id: Int) {
        view.setImageResource(id)
    }
}