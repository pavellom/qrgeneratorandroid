package org.pavellom.qrgenerator.activity

import android.app.AlertDialog
import android.content.ContextWrapper
import android.content.DialogInterface
import android.content.Intent
import android.databinding.Observable
import android.databinding.Observable.OnPropertyChangedCallback
import android.os.Build
import android.os.Bundle
import android.util.SparseBooleanArray
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemClickListener
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import kotlinx.android.synthetic.main.sample_main.*
import org.pavellom.qrgenerator.R
import org.pavellom.qrgenerator.app.Injector
import org.pavellom.qrgenerator.data.Item
import org.pavellom.qrgenerator.data.ViewMode
import org.pavellom.qrgenerator.enum.ItemType
import org.pavellom.qrgenerator.file.LoadFiles
import java.io.File
import java.util.*


class MainActivity() : ExtendActivity() {

    var images = arrayOf<File>()

    private lateinit var editMenuItem: MenuItem
    private lateinit var deleteMenuItem: MenuItem
    private lateinit var revertMenuItem: MenuItem

    private lateinit var deleteDialog: AlertDialog

    val TAG = "MainActivity"

    private val items = Injector.injectItems()
    private val viewMode = Injector.injectViewMode()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        setContentView(R.layout.sample_main)

        list.adapter = MyAdapter()

        add_new_qr.setOnClickListener({
            val intent = Intent(this, AppActivity::class.java)
            startActivity(intent)
        })

        MobileAds.initialize(this,
                getString(R.string.adMob_id))

        adView_main.loadAd(AdRequest.Builder().build())

        setSupportActionBar(toolbar_main)
        getSupportActionBar()!!.setDisplayShowTitleEnabled(false);
        toolbar_title.text = ""
        toolbar_title.textSize = 14f
        LoadFiles.load(this)

        viewMode.readOnly = true

        viewMode.addOnPropertyChangedCallback(object : OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                if ((sender as ViewMode).readOnly) {
                    toolbar_title.text = ""
                } else {
                    toolbar_title.text = getString(R.string.list_selected_items, list.checkedItemCount, list.count)
                    setDeleteButtonEnablement()
                }
                menuItemsSetModeVisibility()
            }
        })

        deleteDialog = AlertDialog.Builder(this)
                .setTitle(R.string.delete_confirm_dialog)
                .setMessage(R.string.delete_confirm_message)
                .setIconAttribute(android.R.attr.alertDialogIcon)
                .setPositiveButton(android.R.string.yes, DialogInterface.OnClickListener { _, _ ->
                    val itemsToDelete = mutableListOf<Item>()
                    list.checkedItemPositions.forEach { pos: Int, isChecked: Boolean ->
                        if (isChecked) {
                            val item = list.getItemAtPosition(pos) as Item
                            itemsToDelete.add(item)
                            list.setItemChecked(pos, false)
                        }
                    }
                    itemsToDelete.forEach({
                        LoadFiles.delete(this, it)
                    })
                    list.adapter = MyAdapter()
                    viewMode.notifyChange()
                })
                .setNegativeButton(android.R.string.no, null).create()
    }

    inline fun SparseBooleanArray.forEach(action: (Int, Boolean) -> Unit) {
        val size = this.size()
        for (i in 0..size - 1) {
            if (size != this.size()) throw ConcurrentModificationException()
            action(this.keyAt(i), this.valueAt(i))
        }
    }

    fun menuItemsSetModeVisibility() {
        deleteMenuItem.setVisible(!viewMode.readOnly)
        revertMenuItem.setVisible(!viewMode.readOnly)
        editMenuItem.setVisible(viewMode.readOnly)
        if (viewMode.readOnly) {
            add_new_qr.show()
        } else {
            add_new_qr.hide()
        }
    }

    fun setDeleteButtonEnablement() {
        deleteMenuItem.setEnabled(if (list.checkedItemCount > 0) true else false)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main_items, menu)
        editMenuItem = menu!!.findItem(R.id.edit)
        deleteMenuItem = menu.findItem(R.id.delete)
        revertMenuItem = menu.findItem(R.id.revert)
        menuItemsSetModeVisibility()
        setDeleteButtonEnablement()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.edit -> {
                setEditMode(false)
            }
            R.id.revert -> {
                setEditMode(true)
            }
            R.id.delete -> {
                deleteDialog.show()
            }
        }
        return true
    }

    fun setEditMode(readOnly: Boolean) {
        list.checkedItemPositions.forEach { pos: Int, _: Boolean ->
            list.setItemChecked(pos, false)
        }
        viewMode.readOnly = readOnly
        viewMode.notifyChange()
    }

    override fun onStart() {
        super.onStart()
        val cw = ContextWrapper(applicationContext)
        if (cw.filesDir.listFiles() != null) {
            images = cw.filesDir.listFiles()
            (list.adapter as BaseAdapter).notifyDataSetChanged()
        }

        list.onItemClickListener = object : OnItemClickListener {
            override fun onItemClick(adapter: AdapterView<*>, v: View, position: Int, id: Long) {
                if (viewMode.readOnly) {
                    val item: Item = adapter.getItemAtPosition(position) as Item
                    val intent = Intent(this@MainActivity, AppActivity::class.java)
                    intent.putExtra("item", item)
                    startActivity(intent)
                } else {
                    viewMode.notifyChange()
                }
            }
        }
    }

    private inner class MyAdapter : BaseAdapter() {

        override fun getCount(): Int {
            return items.size()
        }

        override fun getItem(position: Int): Item {
            return items.getItem(position)
        }

        override fun getItemId(position: Int): Long {
            return items.getItem(position).path().hashCode().toLong()
        }

        override fun getView(position: Int, convertView: View?, container: ViewGroup): View {
            var convertView = convertView
            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.list_item, container, false)
            }

            val item = getItem(position)
            val pathParts = item.path().split("/")
            val fileName = pathParts[pathParts.size - 1]
            val fileType = fileName.split("-")[0]
            val itemType = ItemType.values().find { it.toString().equals(fileType) }

            (convertView?.findViewById(R.id.create_date) as TextView).text = fileName.replace(fileType + "-", "").replace(".png", "")
            (convertView.findViewById(R.id.qr_thumb) as ImageView).setImageResource(itemType?.resourceId
                    ?: ItemType.TEXT.resourceId)

            return convertView
        }
    }
}