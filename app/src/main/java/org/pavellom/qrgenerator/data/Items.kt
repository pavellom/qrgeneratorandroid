package org.pavellom.qrgenerator.data

/**
 * Created by pavellomonosov on 09/01/2018.
 */
class Items {

    var items: ArrayList<Item>

    init {
        items = ArrayList()
    }

    fun reset() {
        items.clear()
    }

    fun addItem(item: Item) {
        items.add(item)
    }

    fun removeItem(item: Item) {
        items.remove(item)
    }

    fun getItem(id: Int): Item = items.get(id)

    fun size(): Int = items.size
}