package org.pavellom.qrgenerator.data

/**
 * Created by pavellomonosov on 30/01/2018.
 */
import android.databinding.BaseObservable

class TextForm(
        var text: String
) : BaseObservable() {
    constructor() : this("")
}