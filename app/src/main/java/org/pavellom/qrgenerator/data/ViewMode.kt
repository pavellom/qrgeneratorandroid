package org.pavellom.qrgenerator.data

/**
 * Created by pavellomonosov on 19/01/2018.
 */
import android.databinding.BaseObservable

class ViewMode(
        var readOnly: Boolean
) : BaseObservable()