package org.pavellom.qrgenerator.data

import android.databinding.BaseObservable

/**
 * Created by pavellomonosov on 16/02/2018.
 */
class Youtube(
        var url: String) : BaseObservable() {
    constructor() : this("")
}