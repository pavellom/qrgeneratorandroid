package org.pavellom.qrgenerator.data

import android.graphics.Bitmap
import android.os.Parcelable
import com.google.auto.value.AutoValue

/**
 * Created by pavellomonosov on 09/01/2018.
 */
@AutoValue
abstract class Item: Parcelable {

    abstract fun path(): String
    abstract fun itemType(): String
    abstract fun img(): Bitmap

    companion object {
        fun create(type: String, path: String, img: Bitmap): Item {
            return builder().itemType(type).path(path).img(img).build()
        }

        fun builder(): Builder = `$AutoValue_Item`.Builder()
    }

    @AutoValue.Builder
    interface Builder {
        fun itemType(s: String): Builder
        fun path(s: String): Builder
        fun img(i: Bitmap): Builder
        fun build(): Item
    }
}