package org.pavellom.qrgenerator.data

import android.databinding.BaseObservable

/**
 * Created by pavellomonosov on 02/03/2018.
 */
class Url(var url: String) : BaseObservable() {
    constructor(): this("")
}