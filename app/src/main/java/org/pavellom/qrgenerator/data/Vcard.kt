package org.pavellom.qrgenerator.data

import android.databinding.BaseObservable

/**
 * Created by pavellomonosov on 16/02/2018.
 */
class Vcard(
        var name: String,
        var title: String,
        var company: String,
        var phoneNumber: String,
        var email: String,
        var address: String,
        var website: String,
        var note: String
) : BaseObservable() {
    constructor() : this("","","","","","","","")
}