package org.pavellom.qrgenerator.data

/**
 * Created by pavellomonosov on 22/12/2017.
 */
data class Event(val eventType: Int) {
    val args: MutableMap<String, Any> = HashMap<String, Any>()

    fun addArg(key: String, value: Any): Event {
        args.put(key, value)
        return this
    }

    companion object {
        val HIDE_PROGRESS: Int = 0
        val NAVIGATE: Int = 1
        val GENERATE_QR = 2
        val UPDATE_PROGRESS = 3
        val FORM_VALID = 4
    }
}