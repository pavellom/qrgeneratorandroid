package org.pavellom.qrgenerator.data

/**
 * Created by pavellomonosov on 30/01/2018.
 */
import android.databinding.BaseObservable

class WifiFormState(
        var label: String,
        var visibility: Int,
        var ssid: String,
        var password: String
) : BaseObservable()